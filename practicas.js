var myFavoriteHero = "Hulk";
var x = 50;
var h = 5;
var y = 10;
var z = h + y;

// --------------------------------------------

const character = {name: 'Jack Sparrow', age: 10};

character.age = 25

var firstName = 'Jon'; 
var lastName = 'Snow'; 
var age = 24; 

console.log('Soy ' + firstName +" "+ lastName + ', tengo ' + age + ' años y me gustan los lobos.');

const toy1 = {name: 'Buss myYear', price: 19};
const toy2 = {name: 'Rallo mcKing', price: 29};

console.log(toy1.price + toy2.price);


let globalBasePrice = 10000;
const car1 = {name: 'BMW m&m', basePrice: 50000, finalPrice: 60000};
const car2 = {name: 'Chevrolet Corbina', basePrice: 70000, finalPrice: 80000};

globalBasePrice = 25000

car1.basePrice += globalBasePrice
car2.basePrice += globalBasePrice


//------------------------------------------------------------------------------


// alert(10 * 5);
// alert(10 / 2 );
// alert(15 % 9);

// var y = 10;
// var z = 5;
// var x = y + z;
// var x = y * z;

// // ------------------------------------------------------------------------------

// const avengers = ["HULK", "SPIDERMAN", "BLACK PANTHER"];

// console.log(avengers[0])

// avengers.splice(0,1,"IRONMAN")

// alert(avengers.length)

// const rickAndMortyCharacters = ["Rick", "Beth", "Jerry"];

// rickAndMortyCharacters.push("Morty","Summer")

// console.log(rickAndMortyCharacters)

// rickAndMortyCharacters.push("Lapiz Lopez")
// rickAndMortyCharacters.pop()

// console.log(rickAndMortyCharacters[0]);
// console.log(rickAndMortyCharacters[4]);

// rickAndMortyCharacters.push("Lapiz Lopez")
// rickAndMortyCharacters.splice(1,1)

// console.log(rickAndMortyCharacters)

// ------------------------------------------------------------------------------

const number1 = 10;
const number2 = 20;
const number3 = 2;

if (number2 / number1 == 2) {
    console.log("number2 dividido entre number1 es igual a 2")
}

if (number1 !== number2) {
    console.log("number1 es estrictamente distinto a number2");
  }
  
  if (number3 != number1) {
    console.log("number3 es distinto number1");
  }
  
  if (number3 *5 == number1) {
    console.log("number3 por 5 es igual a number1");
  }
  
  if (number3 * 5 == number1 && number1 * 2 == number2) {
    console.log("number3 por 5 es igual a number1 Y number1 por 2 es igual a number2");
  }
  
  if (number2 / 2 == number1 || number1 / 5 == number3) {
    console.log("number2 entre 2 es igual a number1 O number1 entre 5 es igual a number3");
  }

//   -------------------------------------------------------------------------

for (let i = 0; i < 10; i++) {
    console.log(i)
}

for (let i = 0; i < 10; i++) {
    
    if (i % 2 == 0) {
        
        console.log(i)
    }

}

for (let i = 0; i <= 10; i++) {
    
    if (i < 10) {
        console.log("Intentando dormir")
    } else {
        console.log("Dormido")
    }
}

